"""
API qui permet de modéliser une partie de mastermind
Une partie de mastermind est caractérisée par :
 - un code secret = une combinaison à découvrir
 - la taille de la combinaison à découvrir
 - le nombre de "couleurs" utilisées dans la partie
 - le nombre de tentatives maximales pour découvrir le code secret
 - la liste des tentatives

Dans cette API, on décide de modéliser une partie de mastermind par un dictionnaire
"""


import combinaison

def creer_jeu(taille_code_secret, nb_couleurs, nb_tentatives):
    code_secret = combinaison.creer_combinaison(taille_code_secret)
    combinaison.genere(code_secret, nb_couleurs)
    print(code_secret)
    return {"taille":taille_code_secret, "nb_couleurs":nb_couleurs,
            "nbTentatives":nb_tentatives, "code":code_secret, "tentatives":[]}

#accesseurs aux propriétés du jeu

def get_taille_code_secret(jeu):
    """Renvoie le nombre de pions utilisés dans le code secret""" 
    return jeu["taille"]

def get_nb_couleurs(jeu):
    """Renvoie le nombre de 'couleurs' utilisées dans la partie""" 
    return jeu["nb_couleurs"]

def get_nb_tentatives(jeu):
    """Renvoie le nombre de tentatives qui ont été réalisées jusqu'à présent""" 
    return jeu["nbTentatives"]

def get_code_secret(jeu):
    """Renvoie le code secret qu'il faut découvrir""" 
    return jeu["code"]

def get_numero_tentatives(jeu):
    """
    Renvoie le numéro de la tentative actuelle :
    la 'première' tentative a le numéro 1
    la deuxième tentative a le numero 2 ... etc
    """ 
    return len(jeu["tentatives"]) + 1


def get_une_tentative(jeu, numero):
    """
    renvoie la tentative du jeu dont le numéro est passé en paramètre
    les tentatives sont numérotées de 1 à n
    """
    return jeu["tentatives"][numero-1]

def affiche_jeu(jeu):
    """
    Permet d'afficher le jeu avec les différentes tentatives déjà effectuées
    Cette fonction ne renvoie rien
    """ 
    print("===========================\nHistorique :" )
    for numero in range(1, get_numero_tentatives(jeu)):
        (proposition, (nb_bien_places, nb_mal_places)) = get_une_tentative(jeu, numero)
        print("tentative", numero, ":", combinaison.to_string(proposition), nb_bien_places, "bien placés et", nb_mal_places, "mal placés")
    print("---------------")
    print("tentative numero ", get_numero_tentatives(jeu))


def nouvelle_tentative(jeu):
    """
    Permet au joueur d'entrer une nouvelle tentative
    Renvoie un tuple contenant (le nombre de pions bien placés, le nombre de pions mal placés)
    """
    proposition = combinaison.creer_combinaison(get_taille_code_secret(jeu))
    combinaison.saisir_combinaison(proposition, get_nb_couleurs(jeu))
    res = combinaison.compare(proposition, get_code_secret(jeu))
    jeu["tentatives"].append((proposition, res))
    return res


def jouer(jeu):
    """permet de lancer une partie de mastermind"""
    fin_du_jeu = False
    while not fin_du_jeu:
        affiche_jeu(jeu)    
        (nb_bien_places, _) = nouvelle_tentative(jeu)
        if nb_bien_places == get_taille_code_secret(jeu):
                print("===========================\nVous avez gagné en", get_numero_tentatives(jeu)-1, 'tentatives')
                fin_du_jeu = True
        elif get_numero_tentatives(jeu) > get_nb_tentatives(jeu):
            print("===========================\nVous avez perdu")
            print("Il fallait trouver ", combinaison.to_string(get_code_secret(jeu)))
            fin_du_jeu = True
    print("Merci et à bientôt !")
    
