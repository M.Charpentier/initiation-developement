"""
API qui permet de modéliser des combinaisons de plusieurs pions de 'couleurs'
Les 'couleurs' sont ici des nombres entiers
"""

import random

def creer_combinaison(taille):
    """
    permet de créer une nouvelle combinaison dont la taille est passé en paramètre 
    (taille = le nombre de pions)
    La combinaison ne contiendra que des 0
    """
    return [0]*taille

def get_nb_pions(la_combinaison):
    """renvoie le nombre de pions de la combinaison"""
    return len(la_combinaison)


def get_pion(la_combinaison, i):
    """ renvoie la couleur/valeur du pion d'indice i de la combinaison"""
    return la_combinaison[i]

def set_pion(la_combinaison, couleur, i):
    """place un pion de la couleur spécifié à la position i de la combinaison"""
    la_combinaison[i] = couleur

def to_string(la_combinaison):
    """renvoie une chaine de caractère qui pourra être utilisée pour visualiser la combinaison"""
    chaine = "|"
    for i in range(get_nb_pions(la_combinaison)):
        chaine += str(get_pion(la_combinaison, i)) + '|'
    return chaine


def genere(la_combinaison, nb_couleurs):
    """
    genère aléatoirement des couleurs pour la combinaison passée en paramètre
    les couleurs seront choisies entre 1 et nb_couleurs
    """
    for i in range(get_nb_pions(la_combinaison)):
        couleur = random.randint(1, nb_couleurs)
        set_pion(la_combinaison, couleur, i)


def saisir_combinaison(la_combinaison, nb_couleurs):
    """
    permet à l'utilisateur d'entrer une combinaison en utilisant que des
    nombres entre 1 et nb_couleurs
    """
    nb_pions = get_nb_pions(la_combinaison)
    ok = False    
    while not ok:
        print("Entrez votre combinaison de ", nb_pions, " pions (compris entre 1 et ", nb_couleurs, ")", sep="")
        print("Séparez la valeur des pions par des espaces")
        proposition = input().split(' ')
        if len(proposition) != nb_pions:
            print("le nombre de pions n'est pas correct")
        else:
            ok = True
            for i in range(len(proposition)):
                try:
                    pion = int(proposition[i])
                    if pion < 1 or pion > nb_couleurs:
                        print("le pion", pion, "n'est pas correct")
                        ok = False
                    else:
                        set_pion(la_combinaison, pion, i)
                except:
                    print("le pion", proposition[i], "n'est pas un entier")
                    ok = False
    


def compare(la_combinaison, code_secret):
    """
    compare la combinaison avec le code secret à trouver
    renvoie un couple dont
      - le premier élément indique le nombre de pions qui sont à la 
        même place dans la combinaison et dans le code secret
      - le deuxième élément indique le nombre de pions de la combinaison
        qui sont aussi dans le code secret mais pas à la bonne place
    Si la combinaison et le code secret n'ont pas le même nombre de pions
    cette fonction renvoie None
    """
    nb_pions = get_nb_pions(la_combinaison)
    
    #si les deux combinaisons ne sont pas comparables car pas de même taille
    if get_nb_pions(code_secret) != nb_pions:
        return None

    #permet de repérer les pions déjà utilisés dans le code secret dans la comparaison
    utilises = [False]*nb_pions
    
    # première phase : on repère les pions bien placés
    nb_bien_places = 0
    for i in range(nb_pions):
        if get_pion(la_combinaison, i) == get_pion(code_secret, i):
            nb_bien_places += 1
            utilises[i] = True

    #deuxième phase; les pions présents mais mal placés
    nb_mal_places = 0
    for i in range(nb_pions):
        check = False
        j = 0
        while j < nb_pions and not check:
            if not utilises[j] and not utilises[i] and get_pion(la_combinaison, i) == get_pion(code_secret, j):
                check = True
                nb_mal_places += 1
                utilises[j] = True
            j += 1
    
    return (nb_bien_places, nb_mal_places)
