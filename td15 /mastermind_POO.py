import combinaison_POO as combinaison

class Mastermind(object):
    def __init__(self,taille_code_secret, nb_couleurs, nb_tentatives):
        self.taille_code_secret = taille_code_secret
        self.nb_couleurs = nb_couleurs
        self.nb_tentatives = nb_tentatives
        self.code_secret = combinaison.Combinaison(taille_code_secret)
        combinaison.Combinaison.genere(self.code_secret, self.nb_couleurs)
        print(self.code_secret)
        self.le_jeu = {"taille":taille_code_secret, "nb_couleurs":nb_couleurs,
            "nbTentatives":nb_tentatives, "code":self.code_secret, "tentatives":[]}

    def get_taille_code_secret(self):
        return self.le_jeu["taille"]
    
    def get_nb_couleurs(self):
        """ Renvoie le nombre de 'couleurs' utilisées dans la partie """ 
        return self.le_jeu["nb_couleurs"]

    
    def get_nb_tentatives(self):
        """Renvoie le nombre de tentatives qui ont été réalisées jusqu'à présent""" 
        return self.le_jeu["nbTentatives"]

    def get_code_secret(self):
        """Renvoie le code secret qu'il faut découvrir""" 
        return self.le_jeu["code"]


    def get_numero_tentatives(self):
        """
        Renvoie le numéro de la tentative actuelle :
        la 'première' tentative a le numéro 1
        la deuxième tentative a le numero 2 ... etc
        """ 
        return len(self.le_jeu["tentatives"]) + 1


    def get_une_tentative(self, numero):
        """
        renvoie la tentative du jeu dont le numéro est passé en paramètre
        les tentatives sont numérotées de 1 à n
        """
        return self.le_jeu["tentatives"][numero-1]

    def affiche_jeu(self):
        """
        Permet d'afficher le jeu avec les différentes tentatives déjà effectuées
        Cette fonction ne renvoie rien
        """ 
        print("===========================\nHistorique :" )
        for numero in range(1, self.get_numero_tentatives()):
            (proposition, (nb_bien_places, nb_mal_places)) = self.get_une_tentative(numero)
            print("tentative", numero, ":", combinaison.Combinaison.to_string(proposition), nb_bien_places, "bien placés et", nb_mal_places, "mal placés")
        print("---------------")
        print("tentative numero ", self.get_numero_tentatives())

    def nouvelle_tentative(self):
        """
        Permet au joueur d'entrer une nouvelle tentative
        Renvoie un tuple contenant (le nombre de pions bien placés, le nombre de pions mal placés)
        """
        proposition = combinaison.Combinaison(self.get_taille_code_secret())
        combinaison.Combinaison.saisir_combinaison(proposition, self.get_nb_couleurs())
        res = combinaison.Combinaison.compare(proposition, self.get_code_secret())
        self.le_jeu["tentatives"].append((proposition, res))
        return res


    def jouer(self):
        """permet de lancer une partie de mastermind"""
        fin_du_jeu = False
        while not fin_du_jeu:
            self.affiche_jeu()    
            (nb_bien_places, _) = self.nouvelle_tentative()
            if nb_bien_places == self.get_taille_code_secret():
                    print("===========================\nVous avez gagné en", self.get_numero_tentatives()-1, 'tentatives')
                    fin_du_jeu = True
            elif self.get_numero_tentatives() > self.get_nb_tentatives():
                print("===========================\nVous avez perdu")
                print("Il fallait trouver ", combinaison.Combinaison.to_string(self.get_code_secret()))
                fin_du_jeu = True
        print("Merci et à bientôt !")
    
