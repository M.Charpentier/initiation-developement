def plus_long_plateau(chaine):
    """recherche la longueur du plus grand plateau d'une chaine

    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """    
    lg_max=0 # longueur du plus grand plateau déjà trouvé
    lg_actuelle=1 #longueur du plateau actuel
    prec='' #caractère précédent dans la chaine
    for i in range (1,len(chaine)):
        if chaine[i]==chaine[i-1]: # si la lettre actuelle est égale à la précédente
            lg_actuelle+=1
        else: # si la lettre actuelle est différente de la précédente
            if lg_actuelle>lg_max:
                lg_max=lg_actuelle
            lg_actuelle=1
    if lg_actuelle>lg_max: #cas du dernier plateau
        lg_max=lg_actuelle
    return lg_max
def test_plus_long_plateau():
    assert plus_long_plateau("aatttttrrreefghhth")==5
    assert plus_long_plateau("aattttrrreefghhhhhhhth")==7
    assert plus_long_plateau("aatttttrrreefghhthhhhhhhhh")==9
    assert plus_long_plateau("tttttrrreefghhth")==5

# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_ville=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]
def villepopu(listevilles,population):
    res,compt=0,0
    for i in range(len(population)):
        if population[i]>res:
            res=population[i]
            compt=i
    return listevilles[compt],population[compt]

print(villepopu(liste_ville,population))
def test_villepopo():
    assert villepopu(["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"],population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725])

def strint(mot):
    nombre=0
    liste=["0","1","2","3","4","5","6","7","8","9",0,1,2,3,4,5,6,7,8,9]
    for i in range(len(mot)):
        for k in range (len(liste)):
            if mot[i]==liste[k]:
                nombre+= liste[k+10]*(10**(len(mot)-(i+1)))
    return nombre
        
def test_strint():
    assert strint("889")==889
    assert strint("8757557")==8757557
    assert strint("0")==0
    assert strint("9999999999999999999999999")==9999999999999999999999999

def recherchemot(liste,lettre):
    li=[]
    for i in liste:
        if i[0]==lettre:
            li.append(i)
    return li
listerechere=["salut","hello","hallo","ciao","hola"]
def test_recherchemot():
    assert recherchemot(listerechere,"h")==["hello","hallo","hola"]
    assert recherchemot(listerechere,"e")==[]
    assert recherchemot(listerechere,"c")==["ciao"]

        
def decoupage(mot):
    mot2=""
    liste=[]
    for i in mot:
        if i.isalpha():
            mot2+=i
        else:
            if mot2!="":
                liste.append(mot2)
            mot2=""
    return liste
def test_decoupage():
    assert decoupage("(3*2)+1")==[]
    assert decoupage("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!")==["Cela","fait","déjà","jours","jours","à","l","IUT","O","Cool"]

def recherchemot2(mot,x):
    liste=decoupage(mot)
    return recherchemot(liste,x)
def test_recherchemot2():
    assert recherchemot2("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!","C")==["Cela","Cool"]

def listebool(n):
    liste=[]
    for i in range(n+1):
        if i==0 or i==1:
            liste.append(False)
        else:
            liste.append(True)
    return liste

def multiple(liste,x):
    for i in range (len(liste)):
        if i%x==0 and i!=x:
            liste[i]=False
    return liste

def crible(nbentier):
    liste=listebool(nbentier)
    for i in range (2,nbentier):
        liste=multiple(liste,i)
    liste=[i for i in range(len(liste)) if liste[i]==True]
    return liste
print(crible(50))
def test_crible():
    assert crible(6)==[2,3,5]
    assert crible(100)==[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
    assert crible(200)==[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199]






















