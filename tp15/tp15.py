def divise_en_singletons ( liste ) :
    """ param : une liste non vide de nombres
    renvoie une liste de listes à un é l é ment , contenant chaque
    é l é ment de liste
    """
    return [[elem] for elem in liste]


def fusionne ( liste1 , liste2 ) :
    """ param : deux listes tri é es dans l ’ ordre croissant
    renvoie la liste tri é e compos é e de tous les é l é ments
    de liste1 et liste2
    """
    indice1,indice2 = 0,0
    liste_final = []
    while indice1 < len(liste1) and indice2 < len(liste2):
        elem1,elem2 = liste1[indice1], liste2[indice2]
        if elem1 <= elem2:
            liste_final.append(elem1)
            indice1 += 1
        else:
            liste_final.append(elem2)
            indice2 += 1
    liste_final.extend(liste1[indice1:])
    liste_final.extend(liste2[indice2:])
    return liste_final

def fusionne_sous_listes ( liste_de_listes ) :
    """
    param : une liste de listes de listes . Les listes internes sont tri é es
    dans l ’ ordre croissant .
    r é sultat : une nouvelle liste de listes , obtenues en fusionnant
    liste_de_listes [0] avec liste_de_listes [1] pour la premi è re ,
    liste_de_listes [2] avec liste_de_listes [3] pour la deuxi è me , etc .
    Si len ( liste_de_listes ) est impair , alors la derni è re sous - liste
    e  st copi é e dans le r é sultat en derni è re position .
    """
    liste_final = []
    for i in range(0, len(liste_de_listes)-1, 2):
        liste_final.append(fusionne(liste_de_listes[i],liste_de_listes[i+1]))
    if len(liste_de_listes) % 2 == 1:
        liste_final.append(liste_de_listes[-1])
    return liste_final
print(fusionne_sous_listes([[5],[7],[8],[9]]))



#Exercice 3 Un problème de sac à dos


exemple1 = { " Abricots ": (1.5 , 152 , 50) , " Marron ": (2.5 , 200 , 100) ,
" Pain ": (0.7 , 112 , 1000) , " Langouste ": (52 , 150 , 500) }
exemple2 ={ " Banane ": (4 , 90 , 500) , " Oeufs ": (0.7 , 150 , 100) ,
" Pomme ": (0.25 , 50 , 1000) , " Raisin ": (3 , 65 , 1000) }

def le_moins_chere(dico):
    def critere(nom):
        return dico[nom][0]
    return min(dico, key = critere)

def meilleur_ratio(dico):
    def critere(nom):
        return dico[nom][1] / dico[nom][0]*10
def calories_max_pour_8_euros(aliments_disponibles):
    pass
