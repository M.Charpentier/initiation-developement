import tp15

def test_divise_en_singletons():
    assert tp15.divise_en_singletons ([6 , 4 , 2 , 7 , 1]) == [[6] , [4] , [2] , [7] , [1]]

def test_fusionne():
    assert tp15.fusionne ([5 , 8 , 9 , 10] , [1 , 2 , 3 , 5 , 17 , 18]) == [1 , 2 , 3 , 5 ,
5 , 8 , 9 , 10 , 17 , 18]

def test_fusionne_sous_listes():
    assert tp15.fusionne_sous_listes ([[4 , 6] , [2] , [5 , 7] ,
[1 , 3] , [0 , 10 , 16]]) == [[2 , 4 , 6] , [1 , 3 , 5 , 7] , [0 , 10 , 16]]
    assert tp15.fusionne_sous_listes ([[4 , 6] , [2] , [5 , 7] ,[1 , 3]]) == [[2 , 4 , 6] , [1 , 3 , 5 , 7]]

def test_le_moins_chere():
    exemple1 = { "Abricots": (1.5 , 152 , 50) , "Marron": (2.5 , 200 , 100) ,
    "Pain": (0.7 , 112 , 1000) , "Langouste": (52 , 150 , 500) }
    exemple2 ={ "Banane": (4 , 90 , 500) , "Oeufs": (0.7 , 150 , 100) ,
    "Pomme": (0.25 , 50 , 1000) , "Raisin": (3 , 65 , 1000) }
    assert tp15.le_moins_chere(exemple1) == "Pain"
    assert tp15.le_moins_chere(exemple2) == "Pomme"



