options=["Charger un fichier","Rechercher la population d'une comune","Afficher la population d'un déparetement","Quitter"]
"""TP7 une application complète
    ATTENTION VOUS DEVEZ METTRE DES DOCSTRING A TOUTES VOS FONCTIONS
"""
def afficher_menu(titre,liste_options):
    """Affiche un menu

    Args:
        titre (str): Un titre
        liste_options (list): une liste d'options 

    Returns:
        str: Retourne un tableau
    """  
    titre=titre.upper()
    print("+",(len(titre)+2)*"-","+")
    print("| ",titre," |")
    print("+",(len(titre)+2)*"-","+")
    for i in range(len(liste_options)):
        print(i+1," -> ",liste_options[i])

def demander_nombre(message,borne_max):
    demande=input(f"{message} [1-{borne_max}]:")
    if demande.isdecimal():
        demande=int(demande)
        if demande<=borne_max and demande>=1:
            return demande
    return False


def menu(titre,liste_options):
    afficher_menu(titre,liste_options)
    return demander_nombre("Entrez votre choix",len(liste_options))


def programme_principal():
    liste_options=["Charger un fichier","Rechercher la population d'une commune","Afficher la liste des commune commencant par ...","Connaitre le nom de la commune la plus peuplé d'un département","Nombre de commune par tranche d'habitants","Avoir top des communes les plus peuplé","Afficher la population de chaque département","Sauvergarder les populations de departement dans un fichier","Quitter"]
    liste_communes=[]
    while True:
        rep=menu("MENU DE MON APPLICATION",liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep==1:
            print("Vous avez choisi",liste_options[rep-1])
            fichier=input("Entrez le nom du fichier que vous souhaitez charger : ")
            liste_communes=charger_fichier_population(fichier)
            print(len(liste_communes))
        elif rep==2:
            print("Vous avez choisi",liste_options[rep-1])
            nomcommune=input("Entre le nom de la commune où vous voulez la populations : ")
            print (population_d_une_commune(liste_communes,nomcommune))
        elif rep==3:
            print("Vous avez choisi",liste_options[rep-1])
            premierelettre=input("Entrez les 3 premieres lettres des communes : ")
            print(liste_des_communes_commencant_par(liste_communes,premierelettre))
        elif rep==4:
            print("Vous avez choisi",liste_options[rep-1])
            numdep=input("Entrez le num du département")
            print(commune_plus_peuplee_departement(liste_communes,numdep))
        elif rep==5:
            print("Vous avez choisi",liste_options[rep-1])
            numdep=input("Entrez la tranche d'habitant séparé par un espace")
            numdep=numdep.split()
            print(nombre_de_communes_tranche_pop(liste_communes,int(numdep[0]),int(numdep[1])))
        elif rep==6:
            print("Vous avez choisi",liste_options[rep-1])
            top=int(input("Combien de villes voulez vous dans la liste"))
            listtop=top_n_population(liste_communes,top)
            print(listtop)
        elif rep==7:
            print("Vous avez choisi",liste_options[rep-1])
            liste_pop_departement=population_par_departement(liste_communes)
            print(liste_pop_departement)
        elif rep==8:
            print("Vous avez choisi",liste_options[rep-1])
            nom_fic=input("Entrez le nom du fichier a crée")
            sauve_population_dpt(nom_fic,liste_pop_departement)
        else:
            break
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!") 



def charger_fichier_population(nom_fic):
    fic=open(nom_fic,"r") # ouvre le fichier nom_fic
    listefinal=[]
    for ligne in fic:
        if ligne!="DEPCOM;COM;PMUN;PCAP;PTOT\n":
            print(ligne)
            liste=ligne.split(";")
            liste[4]=liste[4][:-1]
            del liste[2:4]
            liste[2]=int(liste[2])
            listefinal.append(tuple(liste))
    fic.close()
    return listefinal     
     # fermeture du fichier


def population_d_une_commune(liste_pop,nom_commune):
    for elem in liste_pop:
        if elem[1]==nom_commune:
            return elem[2]
    return None

def liste_des_communes_commencant_par(liste_pop,debut_nom):
    liste_noms=[]
    for elem in liste_pop:
        if elem[1][:3] == debut_nom:
            liste_noms.append(elem[1])
    return liste_noms
            

def commune_plus_peuplee_departement(liste_pop, num_dpt):
    ville=None
    liste2=[elem for elem in liste_pop if elem[0].startswith(num_dpt)]
    for i in range (len(liste2)):
            if ville==None or liste2[i][2]>liste2[ville][2]:
                ville=i
    if ville==None:
        return "Votre departement n'existe pas"
    return liste2[ville][1]


    
def nombre_de_communes_tranche_pop(liste,pop_min,pop_max):
    nbrcomune=0
    for elem in liste:
        if pop_min<=int(elem[2])<=pop_max:
            nbrcomune+=1
    return nbrcomune
def place_top(commune, liste_pop):
    indice=None
    for i in range(len(liste_pop)):
        if int(commune[2])>int(liste_pop[i][2]):
            return i
    return len(liste_pop)
def ajouter_trier(commune,liste,taille_max):
    if len(liste)>taille_max:
        return None
    liste.insert(place_top(commune,liste),commune)
    return liste
    

def top_n_population(liste_pop,nb):
    listetop10=[]
    for elem in liste_pop:
        if len(listetop10)<nb:
            listetop10.append(elem)
        elif elem[2]>listetop10[-1][2]:
            listetop10=ajouter_trier(elem,listetop10,nb)
            del listetop10[-1]
    return listetop10

def population_par_departement(liste_pop):
    liste=[]
    popencours=liste_pop[0][2]
    depencours=liste_pop[0][0][0:2]
    for elem in liste_pop[1:]:
        if elem[0][0:2]==depencours:
            popencours+=elem[2]
        else:
            liste.append((depencours,popencours))
            popencours=elem[2]
            depencours=elem[0][0:2]
    liste.append((depencours,popencours))
    return liste

def sauve_population_dpt(nom_fic,liste_pop_dep):
    fic=open(nom_fic,"w")
    fic.write("NUMDPT,POPDPT\n")
    for elem in liste_pop_dep:
        fic.write(elem[0]+","+str(elem[1])+"\n")
    fic.close()



programme_principal()