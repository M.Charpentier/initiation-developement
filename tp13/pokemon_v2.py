"""
Un pokedex est modélisé par un dictionnaire dont les clefs sont les noms des pokemons 
et les valeurs associée des informations sur des pokemons.
Ces informations sont données sous la forme d'un tuple (familles, attaque, défense, poids).
"""

def mon_pokedex():
    """ renvoie mon pokedex avec la structure de données
        donnée dans la documentaion du module """
    return {'Bulbizarre': ({'Plante', 'Poison'}, 4, 3, 7),
            'Jungko': ({'Plante'}, 7, 1, 52),
            'Herbizarre': ({'Plante', 'Poison'}, 5, 5, 13),            
            'Abo': ({'Poison'}, 4, 2, 6)}

def plus_forte_attaque ( pokedex ) :
    def critere ( nom ) :
        return pokedex [ nom ][1]
    le_plus_fort = max( pokedex , key = critere )
    ( familles , attaque , defense , poids ) = pokedex [ le_plus_fort ]
    return ( le_plus_fort , familles , attaque , defense , poids )

def tri_selon_defense(pokedex):
    def critere ( nom ):
        return pokedex[nom][2]
    return sorted(pokedex, key = critere)

def plus_petite_force(pokedex):
    def critere ( nom ):
        return pokedex[nom][1]+pokedex[nom][2]
    return min(pokedex, key = critere)


def tri_selon_diversite(pokedex):
    def critere1 ( nom ):
        return len(pokedex[nom][0])
    def critere2 (nom):
        return pokedex[nom][1]
    liste = sorted(pokedex, key = critere2)
    liste = sorted(liste, key = critere1)
    new_liste=[]
    for elem in liste:
        ( familles , attaque , defense , poids ) = pokedex [elem]
        new_liste.append((elem , familles , attaque , defense , poids))
    return new_liste

