"""
Un pokedex est modélisé par un dictionnaire dont les clefs sont les noms des pokemons 
et les valeurs associée des informations sur des pokemons.
Ces informations sont données sous la forme d'un dictionnaire.
"""

def mon_pokedex():
    """ renvoie mon pokedex avec la structure de données
        donnée dans la documentaion du module """
    return {'Bulbizarre': {'familles':{'Plante', 'Poison'}, 
                           'attaque':4, 
                           'defense':3, 
                           'poids':7},
            'Herbizarre': {'familles':{'Plante', 'Poison'}, 
                           'attaque':5, 
                           'defense':5, 
                           'poids':13},
            'Jungko': {'familles':{'Plante'}, 
                       'attaque':7, 
                       'defense':1, 
                       'poids':52},
            'Abo': {'familles':{'Poison'}, 
                    'attaque':4, 
                    'defense':2, 
                    'poids':6}}


def plus_forte_attaque(pokedex):
    def critere( nom ):
        return pokedex[nom]["attaque"]
    le_plus_fort = max(pokedex, key = critere)
    ( familles , attaque , defense , poids ) = pokedex [ le_plus_fort ].values()
    return ( le_plus_fort , familles , attaque , defense , poids )


def tri_selon_defense(pokedex):
    def critere( nom ):
        return pokedex[nom]["defense"]
    return sorted(pokedex, key = critere)



def plus_petite_force(pokedex):
    def critere( nom ):
        return pokedex[nom]["attaque"] + pokedex[nom]["defense"]
    return min(pokedex, key = critere)




def tri_selon_diversite(pokedex):
    def critere1 ( nom ):
        return len(pokedex[nom]["familles"])
    def critere2 (nom):
        return pokedex[nom]["attaque"]
    liste = sorted(pokedex, key = critere2)
    liste = sorted(liste, key = critere1)
    new_liste=[]
    for elem in liste:
        ( familles , attaque , defense , poids ) = pokedex [elem].values()
        new_liste.append((elem , familles , attaque , defense , poids))
    return new_liste
