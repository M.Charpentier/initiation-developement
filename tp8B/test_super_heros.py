import super_heros

def test_itelligence_moyenne():
    exemple2 = { "a" :( 1, 1, "a" ),"b" : ( 3, 9, "b"),"c" :( 7, 2, "c") }
    exemple3 = {"a":(1,1,"a"),"b":(3,9,"b"),"d":(4,4,"c")}
    assert super_heros.intelligence_moyenne(exemple2)==4
    assert abs(super_heros.intelligence_moyenne(exemple3)-14/3) <= 0.01

def test_kikelplusfort():
    exemple2 = {"a":(1,1,"a"),"b":(3,9,"b"),"c":(7,2,"c")}
    exemple3 = {"a":(1,1,"a"),"b":(3,9,"b"),"d":(4,4,"c")}
    assert super_heros.kikelplusfort(exemple2)=="c"
    assert super_heros.kikelplusfort(exemple3) == "d"

def test_CombienDeCretins():
    exemple2 = {"a":(1,1,"a"),"b":(3,9,"b"),"c":(7,2,"c")}
    exemple3 = {"a":(1,1,"a"),"b":(3,9,"b"),"d":(4,4,"c")}
    assert super_heros.combienDeCretins(exemple2)== 2
    assert super_heros.combienDeCretins(exemple3) == 2