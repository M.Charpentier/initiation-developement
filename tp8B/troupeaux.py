# TP8 B - Manipuler des listes, ensembles et dictionnaires
troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
troupeau_vide = dict()
troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
mon_troupeau = {"vache":35, "singe":6, "mouton":26}

def total_animaux(troupeau):
    """ Calcule le nombre total d'animaux dans un troupeau

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        int: le nombre total d'animaux dans le troupeau
    """
    ...
    total = 0
    for nb_animaux in troupeau.values():
        total += nb_animaux
    return total
def tous_les_animaux(troupeau):
    """ Détermine l'ensemble des animaux dans un troupeau

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        set: l'ensemble des animaux du troupeau
    """
    animaux=set()
    for nom_animaux in troupeau.keys():
        animaux.add(nom_animaux)
    return animaux


def specialise(troupeau):
    """ Vérifie si le troupeau contient 30 individus ou plus d'un même type d'animal 

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        bool: True si le troupeau contient 30 (ou plus) individus d'un même type d'animal,
        False sinon 
    """
    for nb_animaux in troupeau.values():
        if nb_animaux >= 30:
            return True
    return False


def le_plus_represente(troupeau):
    """ Recherche le nom de l'animal qui a le plus d'individus dans le troupeau
    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        str: le nom de l'animal qui a le plus d'individus  dans le troupeau
        None si le troupeau est vide) """
    max, nom_max = 0, None
    for (nom, nombre) in troupeau.items():
        if nombre > max:
            nom_max, max = nom, nombre
    return nom_max


def quantite_suffisante(troupeau):
    """ Vérifie si le troupeau contient au moins 5 individus de chaque type d'animal

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        bool: True si le troupeau contient au moins 5 individus de chaque type d'animal
        False sinon    """
    for nb_animaux in troupeau.values():
        if nb_animaux < 5:
            return False
    return True

def reunion_troupeaux(troupeau1, troupeau2):
    """ Simule la réunion de deux troupeaux

    Args:
        troupeau1 (dict): un dictionnaire modélisant un premier troupeau {nom_animaux: nombre}
        troupeau2 (dict): un dictionnaire modélisant un deuxième troupeau        

    Returns:
        dict: le dictionnaire modélisant la réunion des deux troupeaux    
    """
    troupeau3 = troupeau1.copy()
    for (nom, nombre) in troupeau2.items():
        if nom not in troupeau3:
            troupeau3[nom] = nombre
        else:
            troupeau3[nom] = troupeau3[nom] + nombre
    return troupeau3
print(reunion_troupeaux(troupeau_de_jean, troupeau_de_perrette))


