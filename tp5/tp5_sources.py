
# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]

#---------------------------------------
# Exemple de scores
#---------------------------------------
liste_scores=[352100,325410,312785,220199,127853]
liste_joueurs=['Batman','Robin','Batman','Joker','Batman']

def mystere(liste,valeur):
    """[summary]

    Args:
        liste ([type]): [Entre une liste de nombre]
        valeur ([type]): [Entre la valeur pour savoir la position de sa 4eme apparition]

    Returns:
        [type]: [Position 4eme appartion de la valeur dans la liste]
    """
    cptval=0
    for i in range(len(liste)):
        if liste[i]==valeur:
            cptval+=1
            if cptval>3:
                return i
    return None


def test_mystere():
    assert mystere([12,5,8,48,1220,20,20,2020,20,418,185,17,5,20,87],20)==13


#2 
def premierchiffre(liste):
    for i in range(len(liste)):
        if liste[i] in "0123456789":
            return i
    return None
def test_premierchiffre():
    assert premierchiffre("on est le 30/09/2021")==10
    assert premierchiffre("on est le vingt-huit")==None


def villepopu(liste_villes,population,ville):
    compteur=None
    for i in range(len(liste_villes)):
        if liste_villes[i]==ville:
            compteur=i
    if compteur==None:
        return None
    return population[compteur]


def test_villepopu():
    assert villepopu(liste_villes,population,"Dreuxx")==None
    assert villepopu(liste_villes,population,"Dreux")==30664
    assert villepopu(liste_villes,population,"Blois")==45871

#3
def trie(liste):
    prec=liste[0]
    for elem in liste[1:]:
        if prec>elem:
            return False
        prec=elem
    return True
print()
def trie():
    assert trie([0,-1,0,5,8,9,10,25,26,98,102])==False
    assert trie([-1,0,5,8,9,10,25,26,98,102])==True
    assert trie([0,-1,0,5,8,7,10,25,26,98,102])==False

def sommeseuil(liste,seuil):
    somme=liste[0]
    for elem in liste[1:]:
        somme+=elem
    if somme>seuil:
        return True
    return False

def adressemail(adresse):
    aro,indice,lastpoint=0,0,0
    for i in range(len(adresse)):
        if adresse[i]==".":
            lastpoint=i
        if adresse[i]==" ":
            return False
        elif adresse[i]=="@":
            aro+=1
            indice=i
    if adresse[0]=="@" or adresse[-1]=="." or aro!=1 or lastpoint<indice:
        return False
    return True
    

def test_adressemail():
    assert adressemail("maxym.charpentier@gmail.com")==True
    assert adressemail("maxym.charpentier@gmailcom")==False
#4
def meilleurscore(scores,joueurs,nom):
    max=None
    for i in range(len(joueurs)):
        if joueurs[i]==nom:
            if max==None:
                max=i
            elif scores[i]>scores[max]:
                max=i
    if max==None:
        return None
    return scores[max]

def test_meilleurscore():
    assert meilleurscore(liste_scores,liste_joueurs,"Batman")==352100
#4.2
def triescores(scores):
    prec=scores[0]
    for elem in scores[1:]:
        if prec<elem:
            return False
        prec=elem
    return True
def test_triescores():
    assert triescores([60,51,41,20,10,6,2,-1])==True
    assert triescores([60,51,41,5,10,6,2,-1])==False
#4.3
def nombrefoisjoueurs(joueurs,nom):
    cpt=0
    for elem in joueurs:
        if elem==nom:
            cpt+=1
    return cpt
def test_nombrefoisjoueur():
    assert nombrefoisjoueurs(['Batman','Robin','Batman','Joker','Batman'],"Batman")==3
    assert nombrefoisjoueurs(['Batman','Robin','Joker','Batman'],"Batman")==2

#4.4
def meilleursclassement(joueurs,nom):
    for i in range(len(joueurs)):
        if joueurs[i]==nom:
            return i+1
    return None
def meilleursclassement2(liste_score,nom,liste_joueur):
    meilleur=meilleurscore(liste_score,liste_joueur,nom)
    liste=[]
    cpt=1
    if meilleur==None:
        return None
    for i in range (len(liste_score)):
        if liste_score[i]==meilleur:
            return cpt
        if liste_joueur[i] not in liste:
            liste.append(liste_joueur[i])
            cpt+=1


print(meilleursclassement2(liste_scores,"Batman",liste_joueurs))

def test_meilleursclassement():
    assert meilleursclassement(['Batman','Robin','Joker','Batman'],"Batman")==1
    assert meilleursclassement(['Robin','Joker',"Joker",'Batman'],"Batman")==4

#4.5

def indiceinserer(score,liste):
    for i in range(len(liste)):
        if score>liste[i]:
            return i
    return(len(liste))
def test_indiceinserer():
    assert indiceinserer(102857,[352100,325410,312785,220199,127853])==5
    assert indiceinserer(1028570,[352100,325410,312785,220199,127853])==0
    assert indiceinserer(302857,[352100,325410,312785,220199,127853])==3


#4.6
def inserer(score,auteur,liste_joueur,liste_score):
    """[summary]

    Args:
        score ([int]): [Entrer le score du joueur]
        auteur ([str]): [Entre le nom de ce joueur]
        liste_joueur ([list]):[Entre une liste je joueuer]
        liste_score ([list]):[Entre une liste de score trie]
    Returns:
        []: [Les 2 listes sont modifier]
    """
    if triescores(liste_score)==False:
        return False
    liste_score.insert(indiceinserer(score,liste_score),score)
    liste_joueur.insert(indiceinserer(score,liste_score),auteur)
