"""Init Dev : TP10"""

# =====================================================================
# Exercice 1 : Choix de modélisation et complexité
# =====================================================================
# Modélisation n°1
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v1 dans le fichier de tests

def appartient_v1(pokemon, pokedex): #O(N)
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for (nom,_) in pokedex:
        if nom == pokemon:
            return True
    return False


def toutes_les_attaques_v1(pokemon, pokedex): #O(N)
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    res = set()
    for (nom,type) in pokedex:
        if nom == pokemon:
            res.add(type)
    return res


def nombre_de_v1(attaque, pokedex):  #O(N)
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    res = 0
    for (_,type) in pokedex:
        if type == attaque:
            res += 1
    return res


def attaque_preferee_v1(pokedex): #O(N)
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    dico=dict()
    for (_,type) in pokedex:
        if type in dico.keys():
            dico[type]+=1
        else:
            dico[type] = 1
    max = None
    type_max = None
    for (type,nb) in dico.items():
        if max is None or nb > max:
            max=nb
            type_max = type
    return type_max

# =====================================================================
# Modélisation n°2
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v2 dans le fichier de tests

def appartient_v2(pokemon, pokedex): #O(1)
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    return pokemon in pokedex.keys()


def toutes_les_attaques_v2(pokemon, pokedex): #O(1)
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    return pokedex[pokemon]


def nombre_de_v2(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    res = 0
    for type in pokedex.values():
        if attaque in type:
            res += 1
    return res


def attaque_preferee_v2(pokedex): #O(N2)
    """ 
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    dico=dict()
    for type in pokedex.values():
        for elem in type:
            if elem in dico.keys():
                dico[elem] += 1
            else:
                dico[elem] = 1
    max = None
    type_max = None
    for (type,nb) in dico.items():
        if max is None or nb > max:
            max = nb
            type_max = type
    return type_max

# =====================================================================
# Modélisation n°3
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v3 dans le fichier de tests


def appartient_v3(pokemon, pokedex): #O(N)
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for nom_poke in pokedex.values():
        if pokemon in nom_poke:
            return True
    return False


def toutes_les_attaques_v3(pokemon, pokedex): #O(N)
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    res = set()
    for (type,nom_poke) in pokedex.items():
        if pokemon in nom_poke:
            res.add(type)
    return res


def nombre_de_v3(attaque, pokedex): #O(1)
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    if attaque in pokedex.keys():
        return len(pokedex[attaque])
    else:
        return 0


def attaque_preferee_v3(pokedex): #O(N)
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    type_max = None
    nb_max = None
    for (attaque,nom_poke) in pokedex.items():
        if nb_max is None or len(nom_poke) > nb_max:
            type_max = attaque
            nb_max = len(nom_poke)
    return type_max

# =====================================================================
# Transformations
# =====================================================================

# Version 1 ==> Version 2

def v1_to_v2(pokedex_v1): #O(N)
    """
    param: prend en paramètre un pokedex version 1
    renvoie le même pokedex mais en version 2
    """
    dico=dict()
    for (poke,attaque) in pokedex_v1:
        if poke in dico.keys():
            dico[poke].add(attaque)
        else:
            dico[poke] = {attaque}
    return dico


# Version  ==> Version 3

def v2_to_v3(pokedex_v2): #O(N)
    """
    param: prend en paramètre un pokedex version2
    renvoie le même pokedex mais en version3
    """
    dico = dict()
    for (nom_poke,ensemble_attaque) in pokedex_v2.items():
        for attaque in ensemble_attaque:
            if attaque in dico.keys():
                dico[attaque].add(nom_poke)
            else:
                dico[attaque] = {nom_poke}
    return dico

# =====================================================================
# Exercice 2 : Ecosystème
# =====================================================================

def extinction_immediate(ecosysteme, animal):
    """
    renvoie True si animal s'éteint immédiatement dans l'écosystème faute
    de nourriture
    """
    return not (ecosysteme[animal] in ecosysteme.keys() or ecosysteme[animal] == None)


def en_voie_disparition(ecosysteme, animal):
    """
    renvoie True si animal s'éteint est voué à disparaitre à long terme
    """
    animal_debut = animal
    cpt = 0
    while animal != None and ecosysteme[animal] != animal_debut and cpt < len(ecosysteme) :
        if extinction_immediate(ecosysteme,animal):
            return True
        animal = ecosysteme[animal]
        cpt += 1
    return False
            
print(en_voie_disparition({ 'Renard':'Poule', 'Poule':'Ver de terre', 'Ver de terre':'Renard', 'Ours':'Renard' },"Ours"))
 

def animaux_en_danger(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en danger d'extinction immédiate"""
    res = set()
    for animal in ecosysteme.keys():
        if extinction_immediate(ecosysteme,animal):
            res.add(animal)
    return res

def especes_en_voie_disparition(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en voués à disparaitre à long terme
    """
    res  =set()
    for animal in ecosysteme.keys():
        if en_voie_disparition(ecosysteme,animal):
            res.add(animal)
    return res
    




def pokemons_par_famille(liste_pokemon):
    dico = dict()
    for (nom,famille,_) in liste_pokemon:
        for petit_famille in famille:
            if petit_famille in dico.keys():
                dico[petit_famille].add(nom)
            else:
                dico[petit_famille] = {nom}
    return dico

ma_liste_pokemon =[("Bulbizarre ", {"Plante ", "Poison "}, "001. png"),("Herbizarre ", {"Plante ", "Poison "}, "002. png"),("Abo", {"Poison "}, "023. png"),("Jungko ", {"Plante "}, "254. png")]
print(pokemons_par_famille(ma_liste_pokemon))



