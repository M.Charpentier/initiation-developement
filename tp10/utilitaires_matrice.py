""" Fonctions utilitaires pour manipuler les matrices """

import API_matrice2 as API


def matrice1():
    m1 = API.construit_matrice(3, 4, None)
    API.set_val(m1, 0, 0, 10)
    API.set_val(m1, 0, 1, 11)    
    API.set_val(m1, 0, 2, 12)
    API.set_val(m1, 0, 3, 13)
    API.set_val(m1, 1, 0, 14)
    API.set_val(m1, 1, 1, 15)
    API.set_val(m1, 1, 2, 16)
    API.set_val(m1, 1, 3, 17)
    API.set_val(m1, 2, 0, 18)
    API.set_val(m1, 2, 1, 19)
    API.set_val(m1, 2, 2, 20)
    API.set_val(m1, 2, 3, 21)
    return m1

def matrice2():
    m2 = API.construit_matrice(2, 3, None)
    API.set_val(m2, 0, 0, 'A')
    API.set_val(m2, 0, 1, 'B')    
    API.set_val(m2, 0, 2, 'C')
    API.set_val(m2, 1, 0, 'D')
    API.set_val(m2, 1, 1, 'E')
    API.set_val(m2, 1, 2, 'F')
    return m2

def matrice3():
    m3 = API.construit_matrice(3, 3, None)
    API.set_val(m3, 0, 0, 2)
    API.set_val(m3, 0, 1, 7)    
    API.set_val(m3, 0, 2, 6)
    API.set_val(m3, 1, 0, 9)
    API.set_val(m3, 1, 1, 5)
    API.set_val(m3, 1, 2, 1)
    API.set_val(m3, 2, 0, 4)
    API.set_val(m3, 2, 1, 3)
    API.set_val(m3, 2, 2, 8)
    return m3

def matrice4():
    m4 = API.construit_matrice(3, 3, None)
    API.set_val(m4, 0, 0, 2)
    API.set_val(m4, 0, 1, 7)    
    API.set_val(m4, 0, 2, 6)
    API.set_val(m4, 1, 0, 9)
    API.set_val(m4, 1, 1, 5)
    API.set_val(m4, 1, 2, 1)
    API.set_val(m4, 2, 0, 4)
    API.set_val(m4, 2, 1, 3)
    API.set_val(m4, 2, 2, 8)
    return m4

def test_get_diagonale_principale():
    m1 = matrice1()
    m2 = matrice2()
    m3 = matrice3()
    assert API.get_diagonale_principale(m1) == None
    assert API.get_diagonale_principale(m2) == None
    assert API.get_diagonale_principale(m3) == [2,5,8]

def somme(matrice1,matrice2):
    nb_lignes=API.get_nb_lignes(matrice1)
    nb_colonnes=API.get_nb_colonnes(matrice1)
    matrice3 = API.construit_matrice(nb_lignes,nb_colonnes,None)
    for i in range (nb_lignes):
        for j in range(nb_colonnes):
            new_valeur = API.get_val(matrice1,i,j)+API.get_val(matrice2,i,j)
            API.set_val(matrice3,i,j,new_valeur)
    return matrice3




def get_ligne(matrice,ligne):
    matrice_ligne=[]
    for i in range (API.get_nb_colonnes(matrice)):
        matrice_ligne.append(API.get_val(matrice,ligne,i))
    return matrice_ligne

def get_colonne(matrice,colonne):
    matrice_colonne=[]
    for i in range(API.get_nb_lignes(matrice)):
        matrice_colonne.append(API.get_val(matrice,i,colonne))
    return matrice_colonne

def charge_matrice_str(nom_fichier):
    """permet créer une matrice de str à partir d'un fichier CSV.

    Args:
        nom_fichier (str): le nom d'un fichier CSV (séparateur  ',')

    Returns:
        une matrice de str
    """
    fic=open(nom_fichier,"r")
    matrice=[]
    nb_lignes=0
    for ligne in fic:
        nb_lignes+=1
        ligne=ligne.split(",")
        nb_colonne=len(ligne)
        ligne[-1]=ligne[-1][:-1]
        for elem in ligne:
            matrice.append(elem)
    res=API.construit_matrice(nb_lignes,nb_colonne,None)
    for i in range(nb_lignes):
        for k in range(nb_colonne):
            API.set_val(res,i,k,matrice[i*nb_colonne+k])
    return res
        
def sauve_matrice(matrice, nom_fichier):
    """permet sauvegarder une matrice dans un fichier CSV.
    Attention, avec cette fonction, on perd l'information sur le type des éléments

    Args:
        matrice : une matrice
        nom_fichier (str): le nom du fichier CSV que l'on veut créer (écraser)

    Returns:
        None
    """
    fichier = open(nom_fichier, "a")
    for i in range (API.get_nb_lignes(matrice)):
        ligne=(get_ligne(matrice,i))
        ligne=(",".join([str(elem) for elem in ligne]))
        fichier.write(f"{ligne}\n")
    fichier.close()
    