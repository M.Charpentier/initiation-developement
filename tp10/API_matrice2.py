def construit_matrice(nb_lignes , nb_colonnes , valeur_par_defaut =0):
    """crée une nouvelle matrice en mettant la valeur par défaut dans chacune de ses cases.

    Args:
        nb_lignes (int): le nombre de lignes de la matrice
        nb_colonnes (int): le nombre de colonnes de la matrice
        valeur_par_defaut : La valeur que prendra chacun des éléments de la matrice

    Returns:
        une nouvelle matrice qui contient la valeur par défaut dans chacune de ses cases
    """
    return [[valeur_par_defaut for k in range (nb_colonnes)] for i in range (nb_lignes)]
print(construit_matrice(4,4))
def get_nb_lignes(matrice):
    """permet de connaître le nombre de lignes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de lignes de la matrice
    """
    return len(matrice)

def get_nb_colonnes(matrice):
    """permet de connaître le nombre de colonnes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de colonnes de la matrice
    """
    return len(matrice[0])

def get_val(matrice, ligne, colonne):
    """permet de connaître la valeur de l'élément de la matrice dont on connaît
    le numéro de ligne et le numéro de colonne.

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)

    Returns:
        la valeur qui est dans la case située à la ligne et la colonne spécifiées
    """
    return matrice[ligne][colonne]
print(get_val(construit_matrice(4,4),3,3))
def set_val(matrice , ligne , colonne , nouvelle_valeur):
    """permet de modifier la valeur de l'élément qui se trouve à la ligne et à la colonne
    spécifiées. Cet élément prend alors la valeur nouvelle_valeur

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)
        nouvelle_valeur : la nouvelle valeur que l'on veut mettre dans la case

    Returns:
        None
    """
    matrice[ligne][colonne] = nouvelle_valeur

def get_ligne(matrice , ligne):
    return matrice[ligne]

def get_colonne(matrice , colonne):
    return [ligne[colonne] for ligne in matrice]

def get_diagonale_principale(matrice):
    if get_nb_colonnes(matrice) != get_nb_lignes(matrice):
        return None
    return [matrice[i][i] for i in range (len(matrice))]
print(construit_matrice(10,10))


def charge_matrice_str(nom_fichier):
    """permet créer une matrice de str à partir d'un fichier CSV.

    Args:
        nom_fichier (str): le nom d'un fichier CSV (séparateur  ',')

    Returns:
        une matrice de str
    """
    fic=open(nom_fichier,"r")
    matrice=[]
    nb_lignes=0
    for ligne in fic:
        nb_lignes+=1
        ligne=ligne.split(",")
        nb_colonne=len(ligne)
        ligne[-1]=ligne[-1][:-1]
        for elem in ligne:
            matrice.append(elem)
    return(nb_lignes,nb_colonne,matrice)
        
def sauve_matrice(matrice, nom_fichier):
    """permet sauvegarder une matrice dans un fichier CSV.
    Attention, avec cette fonction, on perd l'information sur le type des éléments

    Args:
        matrice : une matrice
        nom_fichier (str): le nom du fichier CSV que l'on veut créer (écraser)

    Returns:
        None
    """
    fichier = open(nom_fichier, "a")
    for i in range (get_nb_lignes(matrice)):
        ligne=(get_ligne(matrice,i))
        ligne=(",".join([str(elem) for elem in ligne]))
        fichier.write(f"{ligne}\n")
    fichier.close()
    