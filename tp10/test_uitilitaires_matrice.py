""" tests pour les API matrices 
    Remarques : tous les tests de ce fichier doivent passer quelle que soit l'API utilisée
    """
import utilitaires_matrice as API

def test_get_ligne():
    m1 = API.matrice1()
    m2 = API.matrice2()
    m3 = API.matrice3()
    assert API.get_ligne(m1,0) == [10,11,12,13]
    assert API.get_ligne(m2,1) == ["D","E","F"]

def test_get_colonne():
    m1 = API.matrice1()
    m2 = API.matrice2()
    m3 = API.matrice3()
    assert API.get_colonne(m1,0) == [10,14,18]
    assert API.get_colonne(m2,1) == ["B","E"]


def test_sauve_charge_matrice():
    matrice = API.matrice2()
    API.sauve_matrice(matrice, "matrice.csv")
    matrice_bis = API.charge_matrice_str("matrice.csv")
    assert matrice == matrice_bis