def tri(liste):
    liste = sorted(liste)
    #ICI
    return liste

def fonction1(liste, val):
    if(val <= 20):
        liste.append(val)
    res = tri(liste)
    return res[0]

nb = 19
notes=[18, 5, 3]
res = fonction1(notes, nb)
print(notes)

# représentez l'état de la mémoire à #ICI
# que vaut res et notes à la fin du programme



"""
On décide de représenter une collection d'animaux par un dictionnaire avec comme clé le nom de l'animal 
et en valeur un tupple avec l'espèce, la durée de vie moyenne, l'ensemble des pays ou on peut trouver cet animal
"""

animaux1 = { 'vache' : ('mamifere', 25, {'France', 'Suisse', 'Pays-Bas'}),
             'cigogne' :  ('oiseau', 18, {'France', 'Autriche'}),
             'bouquetin' : ('mamifere', 20, {'France', 'Suisse', 'Italie', 'Autriche'}),
             'girafe': ('mamifere', 15, {'Kenya', 'Congo', 'Afrique_du_Sud'}) }

print(animaux1)

def afficher_animal(collection, animal):
    """ collection un dictionnaire d'animaux
        animal le nom d'un animal
    affiche les caractéristiques de l'animal s'il est dans la collection "pas présent" sinon. """
    ...
    
def max_vie_animal(collection):
    """ collection un dictionnaire d'animaux
    retourne le nom de l'animal qui a la plus grande durée de vie  """
    ...
    
assert(max_vie_animal(animaux1) == ...

def min_pays_animal(collection):
    """ collection un dictionnaire d'animaux
    retourne le nom de l'animal qui vie dans le moins de pays """
    ...
    
assert(min_pays_animal(animaux1) == ...    

def tri_animaux_vie(collection):
    """ collection un dictionnaire d'animaux
    retourne la liste des noms des animaux triées en fonction de leur durée de vie """
    ...
    
assert(tri_animaux_vie(animaux1) == ...  

def tri_animaux_nb_pays(collection):
    """ collection un dictionnaire d'animaux
    retourne la liste des noms des animaux triées en fonction du nombre de pays où ils vivent """
    ...

assert((tri_animaux_nb_pays(animaux1) == ['cigogne', 'vache', 'girafe', 'bouquetin']) or (tri_animaux_nb_pays(animaux1) == ['cigogne', 'girafe', 'vache', 'bouquetin']))
       
def frequence_espece(collection): 
    """ collection un dictionnaire d'animaux
    retourne le dictionnaire de fréquence des espèces """
    ...
    
assert(frequence_espece(animaux1) == ...  

def frequence_pays(collection): 
    """ collection un dictionnaire d'animaux
    retourne le dictionnaire de fréquence des pays """
    ...
    
assert(frequence_pays(animaux1) == ...  
    
def pays_animaux(collection): 
    """ collection un dictionnaire d'animaux
    retourne le dictionnaire qui à chaque pays associe l'ensemble des animaux vivant dans ce pays """
    ...
    
assert(pays_animaux(animaux1) == ...  
    
def tri_pays(collection):
    """ collection un dictionnaire d'animaux
    retourne la liste des noms de pays triées en focntion du nombre d'animaux vivant dans le pays """
    ...
    
assert(tri_pays(animaux1) == ...
