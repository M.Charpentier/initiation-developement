"""
Permet de modéliser un le_plateau de jeu avec :
    - une matrice qui contient des nombres entiers
    - chaque nombre entier correspond à un item :
      MUR, COULOIR, PERSONNAGE, FANTOME
"""
import matrice

MUR = 1
COULOIR = 0
PERSONNAGE = 2
FANTOME = 3

NORD = 'z'
OUEST = 'q'
SUD = 's'
EST = 'd'


def init(nom_fichier="./labyrinthe1.txt"):
    """Construit le plateau de jeu de la façon suivante :
        - crée une matrice à partir d'un fichier texte qui contient des COULOIR et MUR
        - met le PERSONNAGE en haut à gauche cad à la position (0, 0)
        - place un FANTOME en bas à droite
    Args:
        nom_fichier (str, optional): chemin vers un fichier csv qui contient COULOIR et MUR.
        Defaults to "./labyrinthe1.txt".

    Returns:
        le plateau de jeu avec les MUR, COULOIR, PERSONNAGE et FANTOME
    """
    plateau = matrice.charge_matrice(nom_fichier)
    matrice.set_val(plateau,0,0,2)
    matrice.set_val(plateau,matrice.get_nb_lignes(plateau)-1,matrice.get_nb_colonnes(plateau)-1,3)
    return plateau


def est_sur_le_plateau(le_plateau, position):
    """Indique si la position est bien sur le plateau

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        [boolean]: True si la position est bien sur le plateau
    """
    (noligne,nocolonne) = position
    if noligne < 0 or matrice.get_nb_lignes(le_plateau)-1 < noligne:
        return False
    elif 0 > nocolonne or matrice.get_nb_colonnes(le_plateau)-1 < nocolonne:
        return False
    return True


def get(le_plateau, position):
    """renvoie la valeur de la case qui se trouve à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        int: la valeur de la case qui se trouve à la position donnée ou
             None si la position n'est pas sur le plateau
    """
    (noligne,nocolonne) = position
    if not est_sur_le_plateau(le_plateau, position):
        return None
    return matrice.get_val(le_plateau,noligne,nocolonne)


def est_un_mur(le_plateau, position):
    """détermine s'il y a un mur à la poistion donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        bool: True si la case à la position donnée est un MUR, False sinon
    """
    return get(le_plateau,position) == 1


def contient_fantome(le_plateau, position):
    """Détermine s'il y a un fantôme à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est un FANTOME, False sinon
    """
    return get(le_plateau,position) == 3

def est_la_sortie(le_plateau, position):
    """Détermine si la position donnée est la sortie
       cad la case en bas à droite du labyrinthe

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est la sortie, False sinon
    """
    return position == (matrice.get_nb_lignes(le_plateau)-1,matrice.get_nb_colonnes(le_plateau)-1)

def deplace_personnage(le_plateau, personnage, direction):
    """déplace le PERSONNAGE sur le plateau si le déplacement est valide
       Le personnage ne peut pas sortir du plateau ni traverser les murs
       Si le déplacement n'est pas valide, le personnage reste sur place

    Args:
        le_plateau (plateau): un plateau de jeu
        personnage (tuple): la position du personnage sur le plateau
        direction (str): la direction de déplacement SUD, EST, NORD, OUEST

    Returns:
        [tuple]: la nouvelle position du personnage
    """
    (noligne,nocolonne) = personnage
    if direction == SUD:
        new_position = (noligne+1,nocolonne)
    elif direction == EST:
        new_position = (noligne,nocolonne+1)
    elif direction == NORD:
        new_position = (noligne-1,nocolonne)
    elif direction == OUEST:
        new_position = (noligne,nocolonne-1)
    if est_sur_le_plateau(le_plateau, new_position) and not est_un_mur(le_plateau, new_position):
        (newligne,newcolonne) = new_position
        matrice.set_val(le_plateau,noligne,nocolonne,0)
        matrice.set_val(le_plateau, newligne, newcolonne,2)
        return new_position
    return personnage 


def voisins(le_plateau, position):
    """Renvoie l'ensemble des positions cases voisines accessibles de la position renseignées
       Une case accessible est une case qui est sur le plateau et qui n'est pas un mur
    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        set: l'ensemble des positions des cases voisines accessibles
    """
    res = set()
    (noligne,nocolonne) = position
    position_sud = (noligne+1,nocolonne)
    position_est = (noligne,nocolonne+1)
    position_nord = (noligne-1,nocolonne)
    position_ouest = (noligne,nocolonne-1)
    if est_sur_le_plateau(le_plateau, position_sud) and not est_un_mur(le_plateau, position_sud):
        res.add(position_sud)
    if est_sur_le_plateau(le_plateau, position_est) and not est_un_mur(le_plateau, position_est):
        res.add(position_est)
    if est_sur_le_plateau(le_plateau, position_nord) and not est_un_mur(le_plateau, position_nord):
        res.add(position_nord)
    if est_sur_le_plateau(le_plateau, position_ouest) and not est_un_mur(le_plateau, position_ouest):
        res.add(position_ouest)
    return res

def fabrique_le_calque(le_plateau, position_depart):
    """fabrique le calque d'un labyrinthe en utilisation le principe de l'inondation :
       
    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) (1, 0)
    """
    calque = matrice.new_matrice(matrice.get_nb_lignes(le_plateau),matrice.get_nb_colonnes(le_plateau),None)
    matrice.set_val(calque,position_depart[0],position_depart[1],0)
    ensemble_position=set()
    cpt=1
    modif = True
    ensemble_position.add(position_depart)
    while modif:
        modif = False
        ens_voisins=set()
        for pos in ensemble_position:
            ens_voisins=ens_voisins.union(voisins(le_plateau,pos)) 
        ensemble_position = set()
        modif_en_cours=False
        for elem in ens_voisins:
            if get(calque,elem) is None:
                (laligne,lacolonne)=elem
                matrice.set_val(calque,laligne,lacolonne,cpt)
                ensemble_position.add(elem)
                modif = True       
        cpt += 1
    return calque

print(fabrique_le_calque(init(),(0,0)))
def fabrique_chemin(le_plateau, position_depart, position_arrivee):
    """Renvoie le plus court chemin entre position_depart position_arrivee

    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 
        position_arrivee (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        list: Une liste de positions entre position_arrivee et position_depart
        qui représente un plus court chemin entre les deux positions
    """
    calque = fabrique_le_calque(le_plateau, position_depart)
    if matrice.get_val(calque,position_arrivee[0],position_arrivee[1]) is None:
        return None
    parcours=[position_arrivee]
    distance = get(calque,position_arrivee)
    position_en_cours=position_arrivee
    while position_en_cours != position_depart:
        ens_voisin=voisins(le_plateau,position_en_cours)
        for position in ens_voisin:
            if get(calque,position) == (distance - 1):
                parcours.append(position)
                distance -= 1
                position_en_cours = position
                break
    parcours.pop(-1)
    return parcours





def deplace_fantome(le_plateau, fantome, personnage):
    """déplace le FANTOME sur le plateau vers le personnage en prenant le chemin le plus court

    Args:
        le_plateau (plateau): un plateau de jeu
        fantome (tuple): la position du fantome sur le plateau
        personnage (tuple): la position du personnage sur le plateau

    Returns:
        [tuple]: la nouvelle position du FANTOME
    """
    chemin = fabrique_chemin(le_plateau, fantome, personnage)
    if chemin is None:
        return None
    if chemin != []:
        (prochaine_ligne,prochaine_colonne) = chemin[-1]
        matrice.set_val(le_plateau, fantome[0], fantome[1], COULOIR)
        matrice.set_val(le_plateau, prochaine_ligne, prochaine_colonne, FANTOME)
        return (prochaine_ligne, prochaine_colonne)
    else:
        return fantome
