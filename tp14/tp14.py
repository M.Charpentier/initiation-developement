"""TP 14"""

# EXERCICE 1


def recette_la_plus_facile(menu):
    """
    Résultat : le nom de la recette qui est la plus facile
    
    """
    if menu == dict():
        return None
    def critere(nom):
        return menu[nom][1]
    return min(menu, key = critere)
    


def temps_total_de_preparation(menu):
    """
    Résultat : renvoie le temps total de préparation des recettes du menu
    """
    temps_total = 0
    for (temps_preparation, _) in menu.values():
        temps_total += temps_preparation
    return temps_total


def menu_de_chef(menu):
    """
    Résultat : vérifie si le menu contient au moins une recette
    difficile (difficulté > 5)
    
    """
    for (_,difficulte) in menu.values():
        if difficulte > 5:
            return True
    return False


def recette_la_plus_longue(menu):
    """
    Résultat : le nom de la recette qui demande le plus de temps
    de préparation et cuisson
    
    """
    if menu == dict():
        return None
    def critere(nom):
        return menu[nom][0]
    return max(menu, key = critere)
    

def recettes_triee_par_temps(menu):
    """
    Résultat : la liste des noms de recettes triées par ordre croissant
    de temps de préparation    
    """
    if menu == dict():
        return []
    def critere(nom):
        return menu[nom][0]
    return sorted(menu, key = critere)

def recettes_triee_par_difficulte(menu):
    """
    Résultat : la liste des noms de recettes triées par ordre croissant
    de difficulté 
    """
    if menu == dict():
        return []
    def critere(nom):
        return menu[nom][1]
    return sorted(menu, key = critere, reverse = True)


# EXERCICE 2
def tri_porte_monnaie(porte_monnaie):
    return sorted(porte_monnaie, reverse = True)
def reglement(prix, porte_monnaie):
    liste_piece = []
    montant = 0
    new_porte_monnaie = tri_porte_monnaie(porte_monnaie)
    for piece in new_porte_monnaie:
        if montant + piece < prix:
            liste_piece.append(piece)
            montant += piece
        elif montant + piece == prix:
            liste_piece.append(piece)
            return liste_piece
    return None



# EXERCICE 3


def tri_elo(dresseurs):
    def get_elo(nom):
        return nom[1]
    return sorted(dresseurs.items(), key = get_elo)

def duellistes(dresseurs):
    """
    parametre: dresseurs est un dictionnaires :
    - clé : le nom du dresseur (str)
    - valeur : son classement Elo (int)
    résultat : renvoie le nom des deux dresseurs qui ont les classements
    Elo les plus proches
    """
    dresseur_trié = tri_elo(dresseurs)
    ecart = None
    duo = None
    for i in range (0,len(dresseur_trié)-1):
        (nom1,elo1) = dresseur_trié[i]
        (nom2,elo2) = dresseur_trié[i+1]
        if ecart is None or elo2 - elo1 < ecart:
            duo = {nom1, nom2}
            ecart = elo2 - elo1
    return duo


        
