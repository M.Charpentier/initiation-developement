# Codé par Papy Force X, jeune padawan de l'informatique

def dialogue_mot_de_passe():
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        # je vérifie la longueur
        if len(mot_de_passe) < 8:
            longueur_ok = False
        else:
            longueur_ok = True
        # je vérifie s'il y a un chiffre
        chiffre_ok = False
        for lettre in mot_de_passe:
            if lettre.isdigit():
                chiffre_ok = True
        # je vérifie qu'il n'y a pas d'espace
        sans_espace = True
        for lettre in mot_de_passe:
            if lettre == " ":
                sans_espace = False
        # Je gère l'affichage
        if not longueur_ok:
            print("Votre mot de passe doit comporter au moins 8 caractères")
        elif not chiffre_ok:
            print("Votre mot de passe doit comporter au moins un chiffre")
        elif not sans_espace:
            print("Votre mot de passe ne doit pas comporter d'espace")	   
        else:
            mot_de_passe_correct = True        
    print("Votre mot de passe est correct")
    return mot_de_passe



def longueur_ok(mot):
    return len(mot) > 8
def chiffre_ok(mot):
    for lettre in mot:
        if lettre.isdigit():
            return True
    else:
        return False
def sans_espace(mot):
    for lettre in mot:
        if lettre == " ":
            return False
    return True

def dialogue_mot_de_passe2():
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        # je vérifie la longueur
        longueur = longueur_ok(mot_de_passe)
        # je vérifie s'il y a un chiffre
        chiffre = chiffre_ok(mot_de_passe)
        # je vérifie qu'il n'y a pas d'espace
        sans_esp = sans_espace (mot_de_passe)
        # Je gère l'affichage
        if not longueur:
            print("Votre mot de passe doit comporter au moins 8 caractères")
        elif not chiffre:
            print("Votre mot de passe doit comporter au moins un chiffre")
        elif not sans_esp:
            print("Votre mot de passe ne doit pas comporter d'espace")	   
        else:
            mot_de_passe_correct = True        
    print("Votre mot de passe est correct")
    return mot_de_passe


def chiffre_3_ok(mot):
    cpt = 0
    for lettre in mot:
        if lettre.isdigit():
            cpt+=1
    if cpt >= 3:
        return True
    else:
        return False

def chiffre_plus_petit_ok(mot):
    min = None
    cpt = 0
    for lettre in mot:
        if lettre.isdigit():
            if min == None or int(lettre) < min:
                min = int(lettre)
                cpt = 0
            elif min == int(lettre):
                cpt += 1
    if cpt == 0:
        return True
    else:
        return False
print(chiffre_plus_petit_ok("gz5zegzg8zgzg6"))
def chiffre_consequent_ok(mot):
    prec = mot[0]
    for lettre in mot[1:]:
        if lettre.isdigit() and prec.isdigit():
            return False
        prec = lettre
    return True

def charger_fichier():
    liste=[]
    try:
        fichier = open("mdpUltraSecret.txt", "r")
    except:
        return []
    fichier.readline()
    for ligne in fichier:
        ligne=ligne.replace(" ","")
        ligne=ligne.split(":")
        print(ligne)
        ligne[1]=ligne[1][:-1]
        liste.append(ligne)
    fichier.close
    print(liste)
    return liste

def ecrire_fichier(liste):
    fichier = open("mdpUltraSecret.txt", "w")
    fichier.write("NOM MOTDEPASSE\n")
    for (nom,mdp) in liste:
        fichier.write(f"{nom} : {mdp}\n")
    fichier.close

def modifier_fichier(user,mdp):
    liste=charger_fichier()
    existant=False
    for i in range (len(liste)):
        if liste[i][0] == user:
            liste[i][1] = mdp
            existant=True
    if not existant:
        liste.append([user,mdp])
    print(liste)
    ecrire_fichier(liste)
        

def dialogue_mot_de_passe3():
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        # je vérifie la longueur
        longueur = longueur_ok(mot_de_passe)
        # je vérifie s'il y a un chiffre
        chiffre = chiffre_3_ok(mot_de_passe)
        # je vérifie qu'il n'y a pas d'espace
        sans_esp = sans_espace (mot_de_passe)
        chiffre_consequent = chiffre_consequent_ok(mot_de_passe)
        chiffre_plus_petit = chiffre_plus_petit_ok(mot_de_passe)
        # Je gère l'affichage
        if not longueur:
            print("Votre mot de passe doit comporter au moins 8 caractèress")
        elif not chiffre:
            print("Votre mot de passe doit comporter au moins 3 chiffre")
        elif not sans_esp:
            print("Votre mot de passe ne doit pas comporter d'espace")	   
        elif not chiffre_consequent:
            print("2 chiffres consecutif")	
        elif not chiffre_plus_petit:
            print("2 fois petit chiffre")	
        else:
            mot_de_passe_correct = True        
    print("Votre mot de passe est correct")
    modifier_fichier(login,mot_de_passe)
    return mot_de_passe


