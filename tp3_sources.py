# exercice 2

def mystere_exo2(entree):
    """[summary]

    Args:
        entree ([liste]): [Liste d'entier]

    Returns:
        [bool]: [Return True si il y plus nombre pair false sinon]
    """
    paire=0
    impaire=0
    # au début de chaque tour de boucle
    #  A COMPLETER
    for i in entree:
        if i%2==0:
            paire+=1
        else:
            impaire+=1
    return paire>=impaire
def test_mystere_exo2():
    assert mystere_exo2([1,4,6,-2,-5,3,10])==True
    assert mystere_exo2([1,4,6,-2,-5,3,65,10])==True
    assert mystere_exo2([42,3065])==True
    assert mystere_exo2([9,-1,3,8,-6])==False

# exercice 3

def min_sup(liste_nombres,valeur):
    """trouve le plus petit nombre d'une liste supérieur à une certaine valeur

    Args:
        liste_nombres (list): la liste de nombres
        valeur (int ou float): la valeur limite du minimum recherché

    Returns:
        int ou float: le plus petit nombre de la liste supérieur à valeur
    """
    res = None
    # au début de chaque tour de boucle res est le plus petit élément déjà énuméré
    # supérieur à valeur

    for elem in liste_nombres:
        if res== None:
            if elem > valeur:
                res=elem
        elif elem>valeur and elem<res:
            res=elem
    return res

print(min_sup([-2,-5,2,9.8,-8.1,7],0))

def test_min_sup():
    assert min_sup([8,12,7,3,9,2,1,4,9],5)==7
    assert min_sup([-2,-5,2,9.8,-8.1,7],0)==2
    assert min_sup([5,7,6,5,7,3],10)==None
    assert min_sup([],5)==None

# exercice 4
def nb_mots ( phrase ) :
    """Fonction qui compte le nombre de mots d'une phrase

    Args:
        phrase (str): une phrase dont les mots sont séparés par des espaces (éventuellement plusieurs)

    Returns:
        int: le nombre de mots de la phrase
    """    
    resultat =0
    c1 = ''
    # au début de chaque tour de boucle
    # c1 vaut
    # c2 vaut
    # resultat vaut
    if phrase=="":
        return 0
    if phrase[0]==' ':
        resultat-=1
    for c2 in phrase :
        if c1 == ' ' and c2 != ' ':
            resultat = resultat +1
        c1 = c2
    return resultat+1
def test_nb_mots():
    assert nb_mots("bonjour, il fait beau")==4
    assert nb_mots("houla!     je    mets beaucoup   d'  espaces    ")==6
    assert nb_mots(" ce  test ne  marche pas ")==5
    assert nb_mots("")==0 #celui ci non plus
print(nb_mots(" ce  test ne  marche pas "))


#5.1
def sommepair(liste):
    somme=0
    for i in liste:
        if i%2==0:
            somme+=i
    return somme

def test_sommepair():
    assert(sommepair([12,13,6,5,7])==18)
    assert(sommepair([4,13,2,5,7])==6)
    assert(sommepair([11,13,69,5,7])==0)


#5.2 
def dernierevoyelle(mot):
    lettre=None
    for i in mot:
        if i in "aeiouy":
            lettre=i
    return lettre
print(dernierevoyelle("hgjk"))
def test_dernierevoyelle():
    assert(dernierevoyelle("buongiorno")=="o")
    assert(dernierevoyelle("bonjour")=="u")
    assert(dernierevoyelle("hgjgkghjhjgjkkhgjh")=="")
    assert(dernierevoyelle("opzefgjiqzejfk,lefkzopjpajofzgjzmojaryer")=="e")
#5.3
#5.3

def proportionnega(liste):
    compt=0
    for i in liste:
        if i<0:
            compt+=1
    if len(liste)==0:
        return None
    else:
        return compt/len(liste)

def test_propotionnega():
    assert(proportionnega([4,-2,8,2,-2,-7])==0.5)
    assert(proportionnega([4,2,8,2,2,7])==0)
    assert(proportionnega([        ])==None)
#6

def sommeentier(n):
    """Fonction qui fait la somme des n premiers entier

    Args:
       n (int) : Somme jusqu'a quel entier 

    Returns:
        int: Somme des n premiers entiers
    """   
    if n==0:
        return 0
    else:
        return n+sommeentier(n-1)


def test_someentier():
    assert(sommeentier(4)==10)
    assert(sommeentier(0)==0)
    assert(sommeentier(10)==55)
    assert(sommeentier(100)==5050)

def suite(val_init,n):

    """Fonction qui fait la somme des n premiers thermes de la suite avec val_init comme premier therme

    Args:
       val_init (int) : Valeur initiale
        n (int) : jusqu'a quel valeur aller

    Returns:
        int: Somme des n premiers thermes de la suite
    """ 
    for i in range(n):
        if val_init%2==0:
            val_init=val_init/2
        else:
            val_init=3*val_init+1
    return int(val_init)
print(suite(6,3))

def test_suite():
    assert suite(6,0)==6
    assert suite(6,1)==3
    assert suite(6,2)==10
    assert suite(6,3)==5           
    assert suite(6,4)==16               


#Ex7
def sommeliste(liste):
    res=0
    for i in liste:
        res+=i
    return res


def maximum(liste):
    res=liste[0]
    for i in liste [1:]:
        if i > res:
            res=i
    return res

def minimum(liste):
    res=liste[0]
    for i in liste [1:]:
        if i < res:
            res=i
    return res

def ecart(liste):
    res=maximum(liste)-minimum(liste)
    return res

def superieur(liste):
    res = 0
    for i in liste:
        if i>10:
            res+=1
    return res

def moyenne(liste):
    compt,res=0,0
    if i < 0:
        compt+=i
        res+=1
    return res/comp

def syllabe(mot):
    total=0
    if mot[0] in "aeiouy":
        total+=1
    for i in range(1,len(mot)):
        a=mot[i-1]
        b=mot[i]
        if a not in "aeiouy" and b in "aeiouy":
            total+=1
    return total
print(syllabe("eau"))