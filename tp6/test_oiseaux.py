import oiseaux
# --------------------------------------
# FONCTIONS
# --------------------------------------

def test_recherche_oiseau():
    assert oiseaux.recherche_oiseau(oiseaux.oiseaux,"Pie")==('Pie', 'Corvidé')
    assert oiseaux.recherche_oiseau(oiseaux.oiseaux,"Piez")==None

def test_recherche_par_famille():
    assert oiseaux.recherche_par_famille(oiseaux.oiseaux,"Passereau")==[('Mésange', 'Passereau'), ('Moineau', 'Passereau'), ('Pinson', 'Passereau'), ('Rouge-gorge', 'Passereau')]

def test_oiseau_le_plus_observe():
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations1)=="Moineau"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations2)=="Tourterelle"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations3)=="Mésange"
    assert oiseaux.oiseau_le_plus_observe([])==None

def test_est_liste_observations():
    assert oiseaux.est_liste_observations(oiseaux.observations1)==False
    assert oiseaux.est_liste_observations(oiseaux.observations3)==True

def test_max_observations():
    assert oiseaux.max_observations(oiseaux.observations1)==5

def test_moyenne_oiseaux_observes():
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.observations1)==3

def test_total_famille():
    assert oiseaux.total_famille(oiseaux.observations2,oiseaux.oiseaux,"Passereau")==8
    assert oiseaux.total_famille(oiseaux.observations1,oiseaux.oiseaux,"Passereau")==8
    assert oiseaux.total_famille(oiseaux.observations3,oiseaux.oiseaux,"Picidae")==2

def test_construire_liste_observations():
    assert oiseaux.construire_liste_observations(oiseaux.oiseaux,oiseaux.comptage1)==[('Merle', 2), ('Moineau', 5), ('Pic vert', 1), ('Pie', 2), ('Tourterelle', 5), ('Rouge-gorge', 3)]
    assert oiseaux.construire_liste_observations(oiseaux.oiseaux,oiseaux.comptage2)==[("Merle",2), ("Mésange",1), ("Moineau",3), ("Pinson",3),("Tourterelle",5), ("Rouge-gorge",1)]



def test_creer_ligne_sup():
    assert oiseaux.creer_ligne_sup(oiseaux.observations1,2)=="**  **      **  **  **  "

def test_creer_ligne_noms_oiseaux():
    assert oiseaux.creer_ligne_noms_oiseaux(oiseaux.observation1)=="Mer Moi Pic Pie Tou Rou "



