# --------------------------------------
# DONNEES
# --------------------------------------

# exemple de liste d'oiseaux observables
oiseaux=[
        ("Merle","Turtidé"), ("Mésange","Passereau"), ("Moineau","Passereau"), 
        ("Pic vert","Picidae"), ("Pie","Corvidé"), ("Pinson","Passereau"),
        ("Tourterelle","Colombidé"), ("Rouge-gorge","Passereau")
        ]
# exemples de listes de comptage ces listes ont la même longueur que oiseaux
comptage1=[2,0,5,1,2,0,5,3]
comptage2=[2,1,3,0,0,3,5,1]
comptage3=[0,4,0,3,2,1,4,2]

# exemples de listes d'observations. Notez que chaque liste correspond à la liste de comptage de
# même numéro
observations1=[
        ("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2), 
        ("Tourterelle",5), ("Rouge-gorge",3)
            ]

observations2=[
        ("Merle",2), ("Mésange",1), ("Moineau",3), 
        ("Pinson",3),("Tourterelle",5), ("Rouge-gorge",1)
            ]

observations3=[
        ("Mésange",4),("Pic vert",2), ("Pie",2), ("Pinson",1),("Rouge-gorge",2),("Tourterelle",4)]

# --------------------------------------
# FONCTIONS
# --------------------------------------

def oiseau_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    oiseau_max=None
    for i in range (len(liste_observations)):
        if oiseau_max==None or liste_observations[i][1]>oiseau_max[1]:
            oiseau_max=liste_observations[i]
    if oiseau_max == None:
        return None
    return oiseau_max[0]
    

#--------------------------------------
# PROGRAMME PRINCIPAL
#--------------------------------------

#afficher_graphique_observation(construire_liste_observations(oiseaux,comptage3))
#comptage=saisie_observations(oiseaux)
#afficher_graphique_observation(comptage)
#afficher_observations(comptage,oiseaux)

def recherche_oiseau(liste_oiseau,nom_oiseau):
    for elem in liste_oiseau:
        if elem[0]==nom_oiseau:
            return elem
    return None



def recherche_par_famille(liste_oiseau,nom_famille):
    liste=[]
    for elem in liste_oiseau:
        if elem[1]==nom_famille:
            liste.append(elem)
    return liste

#3

def est_liste_observations(liste_observation):
    prec=liste_observation[0][0]
    for elem in liste_observation:
        if elem[1]==0:
            return False
        if elem[0]<prec:
            return False
        prec=elem[0]
    return True

def max_observations(liste_observation):
  max=liste_observation[0]
  for elem in liste_observation[1:]:
        if elem[1]>max[1]:
            max=elem
  return max[1]


def moyenne_oiseaux_observes(liste_observation):
    moyenne=0
    for elem in liste_observation:
        moyenne+=elem[1]
    return moyenne/(len(liste_observation))

def total_famille(liste_observation,liste_famille,famille):
    total=0
    liste=recherche_par_famille(liste_famille,famille)
    for elem in liste_observation:
        if recherche_oiseau(liste_famille,elem[0]) in liste:
            total+=elem[1]
    return total
print(total_famille(observations1,oiseaux,"Corvidé"))
#4

def construire_liste_observations(liste_oiseaux,liste_comptage):
    liste_observation=[]
    for i in range(len(liste_comptage)):
        if liste_comptage[i]!=0:
            liste_observation.append((liste_oiseaux[i][0],liste_comptage[i]))
    return liste_observation

def cree_observation(liste_oiseaux):
    liste=[]
    liste_observation=[]
    for i in range(len(liste_oiseaux)):
        a=int(input(f"Combien avez vous vu de {liste_oiseaux[i][0]} : "))
        if a!=0:
            liste_observation.append((liste_oiseaux[i][0],a))
    return liste_observation


#5

def affichage(liste_oiseaux,liste_observation):
    for elem in liste_observation:
        nom=elem[0].ljust(20)
        famille=recherche_oiseau(liste_oiseaux,elem[0])[1].ljust(20)
        nombre=str(elem[1]).ljust(20)
        print("Nom :",nom,"Famille : ",famille,"Nb Obervés : ",nombre)

def cree_ligne_sup(liste_observation,seuil):
    chaine=""
    for elem in liste_observation:
        if elem[1]>=seuil:
            chaine+="**  "
        else:
            chaine+="    "
    return chaine
def creer_ligne_noms_oiseaux(liste_observation):
    chaine=""
    for elem in liste_observation:
        chaine+=elem[0][0:3]+" "
    return chaine
def graphique(liste_observation):
    a=max_observations(liste_observation)
    for i in range(a,0,-1):
        print(cree_ligne_sup(liste_observation,i))
    print(creer_ligne_noms_oiseaux(liste_observation))

graphique(observations1)
